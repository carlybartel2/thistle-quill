/*@codekit-append "../sections/form/form.js"*/

var site;

function Site () {

	var instance, jQueries, internal;
	instance = this;
	internal = {};
	jQueries = {};

	function onReady ( _event ) {
	  window.isReady = true;
	  jQuery ('.form-section').each(createFormSection);
	}
	
	function createFormSection ( _index, _element ) {
	  var _module;
	  _module = new FormSection();
	  _module.setElementJQuery ( _element );
	  _module.init();
  }
	
	jQuery( onReady );

}

site = new Site();