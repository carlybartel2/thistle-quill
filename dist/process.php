<?php
    
    function getResultsFromArray ( $ourArray ) {
      $result = '';
      foreach($ourArray as $ourKey => $ourValue ):
        $result .= $ourValue . "\n\n";
      endforeach;
      return $result;
    }

    $to = "hello@thistle-and-quill.com"; 
    $from = $_REQUEST['email']; 
    $name = $_REQUEST['name']; 
    $headers = "From: $from"; 
    $subject = "Custom Work Submission"; 
    $fields = array(); 
    $fields{"firstname"} = "First name"; 
    $fields{"lastname"} = "Last name"; 
    $fields{"email"} = "Email"; 
    $fields{"type"} = "type";    
    $fields{"message"} = "Message";
    
    $body = "Thistle + Quill Custom Work Submission:\n\n";
    
    foreach($fields as $a => $b):
      if( $a === 'type' ):
        $body .= 'Type:' . getResultsFromArray ( $_REQUEST['type'] );
      else:
        $body .= $b . ': ' . $_REQUEST[$a] . "\n\n";
      endif;
    endforeach;
    $send = mail($to, $subject, $body, $headers);
?>